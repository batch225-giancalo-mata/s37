// Contains all the endpoints for our application.

// We need to use express's Router() function to achieve this.

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application.

const router = express.Router();

const taskController = require("../controllers/taskController");

/*
	Syntax : localhost:3001/tasks/getinfo

*/

// [SECTION] Routes

// Route to get all the task
router.get("/getinfo", (req, res) => {

	taskController.getAllTasks().then(
		resultFromController => res.send(resultFromController));
})

// Route to create a new task
router.post("/create", (req, res) => {

	// If information will be coming from client side commonly from forms, the data can be accessed from the request "body" property.
	taskController.createTask(req.body).then(result => res.send(result));
})

// Route to delete a task
// The colon (:) is an identifier that helps create a dynamic route which allows us to supply information in the URL.
// The word that comes after the colon (:) symbol will be the name of the URL parameter.
// ":id" is a wildcard where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL.

/*
	Ex.
	localhost:3001/tasks/delete/1234abc5678efg0
	the 1234 is assigned to the "id" parameter in the URL.
*/

router.delete("/delete/:id", (req, res) => {

	// URL parameter values are accessed via the request object's "params" property
	// The property name of this object will match the given URL parameter name.
	// In this case "id" is the name of the parameter.
	// If the information will be coming from the URL, the data can be accessed from the request "params" property.

	taskController.deleteTask(req.params.id).then(result => res.send(result));

})

// Router to updated task
router.put("/update/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(result => res.send(result));
})


// Use "module.exports" to export the router object to use in the "app.js"

router.get("/:id", (req, res) => {
	
	taskController.getUserTasks(req.params.id).then(
		result => res.send(result));
})

router.get("/:id/complete", (req, res) => {
	
	taskController.updateStatusToComplete(req.params.id).then(
		result => res.send(result));
})

module.exports = router;

// ================= ACTIVITY ======================


